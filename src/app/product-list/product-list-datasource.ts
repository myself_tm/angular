import { DataSource } from '@angular/cdk/collections';
import { MatPaginator, MatSort } from '@angular/material';
import { map } from 'rxjs/operators';
import { Observable, merge } from 'rxjs';
import { Select } from '@ngxs/store';
import { ProductState, Product } from '../shared';


/**
 * Data source for the Products view
 */
export class ProductsDataSource extends DataSource<Product> {

  @Select(ProductState.getProducts) stateProducts$: Observable<Product[]>

  productList: Product[];

  constructor(private paginator: MatPaginator, private sort: MatSort) {
    super();
    this.stateProducts$.subscribe(productList => {
      this.productList = productList;

      this.paginator.length = this.productList.length;
    })
  }

  /**
   * Connect this data source to the table. The table will only update when
   * the returned stream emits new items.
   * @returns A stream of the items to be rendered.
   */
  connect(): Observable<Product[]> {
    const dataMutations = [
      this.stateProducts$,
      this.paginator.page,
      this.sort.sortChange
    ];

    return merge(...dataMutations).pipe(map(() => {
      return this.getPagedData(this.getSortedData([...this.productList]));
    }));
  }

  disconnect() { }

  private getPagedData(data: Product[]) {
    const startIndex = this.paginator.pageIndex * this.paginator.pageSize;
    return data.splice(startIndex, this.paginator.pageSize);
  }

  private getSortedData(data: Product[]) {
    if (!this.sort.active || this.sort.direction === '') {
      return data;
    }

    return data.sort((a, b) => {
      const isAsc = this.sort.direction === 'asc';
      switch (this.sort.active) {
        case 'name': return compare(a.name, b.name, isAsc);
        case 'id': return compare(+a.id, +b.id, isAsc);
        default: return 0;
      }
    });
  }
}

function compare(a, b, isAsc) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}
